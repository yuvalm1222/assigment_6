#include "MathUtils.h"


double MathUtils::CalHexagonArea(double side)
{
	return 3 * sqrt(3)*side*side / 2;
}

double MathUtils::CalPentagonArea(double side)
{
	return sqrt(5 * (5 + 2 * sqrt(5))) * side*side / 4;
}
