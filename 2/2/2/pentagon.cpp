#include "pentagon.h"



pentagon::pentagon(std::string color, std::string name, double side) : Shape(color, name)
{
	_side = side;
}


pentagon::~pentagon()
{
}

void pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "side is: " << _side << std::endl << "area is: " << CalArea() << std::endl;;
}

double pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(_side);
}

void pentagon::setSide(double newSide)
{
	_side = newSide;
}
