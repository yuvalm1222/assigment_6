#pragma once
#include <math.h>
class MathUtils
{
public:
	static double CalHexagonArea(double side);
	static double CalPentagonArea(double side);
};

