#include "hexagon.h"



hexagon::hexagon(std::string color, std::string name, double side) : Shape(color,name)
{
	_side = side;
}


hexagon::~hexagon()
{
}

double hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(_side);
}

void hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "side is: " << _side << std::endl << "area is: " << CalArea() << std::endl;
}



void hexagon::setSide(double newSide)
{
	_side = newSide;
}
