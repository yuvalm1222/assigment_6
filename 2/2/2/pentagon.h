#pragma once
#include "shape.h"
#include "MathUtils.h"
class pentagon : public Shape
{
private:
	double _side;
public:
	pentagon(std::string color, std::string name, double side);
	~pentagon();
	virtual void draw();
	virtual double CalArea();
	void setSide(double newSide);

};

