#pragma once
#include "shape.h"
#include "MathUtils.h"
class hexagon : public Shape
{
private:
	double _side;
public:
	hexagon(std::string color, std::string name, double side);
	~hexagon();
	virtual void draw();
	virtual double CalArea();
	void setSide(double newSide);
};

