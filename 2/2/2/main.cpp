#include <iostream>
#include <iomanip> 
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "hexagon.h"
#include "pentagon.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"
#include "MathUtils.h"
int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0; double side = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	hexagon hex(col, nam, side);
	pentagon pent(col, nam, side);



	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrhex = &hex;
	Shape *ptrpent = &pent;


	std::exception* ShapeExcept = new shapeException();//Omer: Exceptions should be static
	std::exception* InputExcept = new InputException();//Omer: where is the delete?

	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; std::string shapetype;
	char x = 'y';
	 do{
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram , e = pentagon , f = hexagon" << std::endl;
		std::cin >>  shapetype;
		if (shapetype.length()> 1) // incase the user has enterd more than 1 letter as an input, we print him a warning and use the first letter enterd
		{
			std::cout << "Warning - dont try to build more than one shape at once!" << std::endl;
			shapetype = shapetype[0]; //shape type will be the the first letter enterd.
		}
	 try
		{

			switch (shapetype[0]) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;	
				std::cin >> col >> nam >> rad;
				if (std::cin.fail())
				{
					throw(InputException());
				}
				if (rad < 0)
				{
					throw(shapeException());
				}
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					throw(InputException());
				}
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					throw(InputException());
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail())
				{
					throw(InputException());
				}
				if (ang < 0 || ang > 180 || ang2 < 0 || ang2 > 180)
				{
					throw(shapeException());
				}
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'e':
				std::cout << "enter name,color,side" << std::endl;
				std::cin >> nam >> col >> side;
				if (std::cin.fail())
				{
					throw(InputException());
				}
				pent.setName(nam);
				pent.setColor(col);
				pent.setSide(side);
				ptrpent->draw();
				break;
			case 'f':
				std::cout << "enter name,color,side" << std::endl;
				std::cin >> nam >> col >> side;
				if (std::cin.fail())
				{
					throw(InputException());
				}
				hex.setName(nam);
				hex.setColor(col);
				hex.setSide(side);
				ptrhex->draw();
				break;
			case 'x':
				std::cout << "bye!";
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
		}
		catch (InputException)//Omer: catch by reference
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			printf(InputExcept->what());
		}
		catch (shapeException)
		{			
			printf(ShapeExcept->what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
		if (shapetype[0] != 'x')
		{
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> shapetype;
		}
	 } while (shapetype[0] != 'x');



		system("pause");
		return 0;
	
}